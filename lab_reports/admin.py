from django.contrib import admin

# Register your models here.
from django.contrib import admin

from .models import Site, Report, Test

admin.site.register(Site)
admin.site.register(Report)
admin.site.register(Test)