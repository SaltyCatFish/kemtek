from django.apps import AppConfig


class LabReportsConfig(AppConfig):
    name = 'lab_reports'
