from django import forms
from django.contrib.auth.forms import AuthenticationForm


from .models import Test, Report

class TestForm(forms.ModelForm):

    class Meta:
        model = Test
        fields = ('description', 'measurement', 'residuals', 'parameters')

class ReportDateForm(forms.ModelForm):
    report_date = forms.DateField(widget=forms.TextInput(attrs={
        'id': 'datepicker',
        'class': 'form-control',
        'placeholder': 'Date of report',
        })) 
    class Meta:
        model = Report
        fields = ['report_date']

    def clean_report_date(self):
        return self.cleaned_data['report_date']

class ReportRemarksForm(forms.Form):
    remarks = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control',
        'placeholder': 'Enter remarks here',
        }))

    def clean_report_remarks(self):
        return self.cleaned_data['remarks']



    