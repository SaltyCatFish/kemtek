# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-17 18:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='date updated')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('remarks', models.TextField()),
                ('report_date', models.DateTimeField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='date updated')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('name', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=200)),
                ('location', models.CharField(max_length=200)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Test',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_updated', models.DateTimeField(auto_now=True, verbose_name='date updated')),
                ('date_created', models.DateTimeField(auto_now_add=True, verbose_name='date created')),
                ('description', models.CharField(choices=[('PAK', 'P Alkalinity'), ('MAK', 'M Alkalinity'), ('CHL', 'Chlorides'), ('HRD', 'Hardiness'), ('NIT', 'Nitrite'), ('PHO', 'Phosphate'), ('SUL', 'Sulfite'), ('PH', 'Ph'), ('SUS', 'Suspended Solids')], max_length=3)),
                ('measurement', models.CharField(max_length=200)),
                ('residuals', models.CharField(max_length=200)),
                ('parameters', models.CharField(blank=True, max_length=200)),
                ('report', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lab_reports_test_report', to='lab_reports.Report')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='report',
            name='site',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='lab_reports_report_site', to='lab_reports.Site'),
        ),
    ]
