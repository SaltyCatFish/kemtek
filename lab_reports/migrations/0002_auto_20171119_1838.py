# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-19 23:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_reports', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='test',
            name='description',
            field=models.CharField(choices=[('PAK', 'P Alkalinity'), ('MAK', 'M Alkalinity'), ('CHL', 'Chlorides'), ('HRD', 'Hardness'), ('NIT', 'Nitrite'), ('PHO', 'Phosphate'), ('SUL', 'Sulfite'), ('PH', 'Ph'), ('SUS', 'Suspended Solids')], max_length=3),
        ),
    ]
