from django.db import models
from django.utils import timezone
from django.core.urlresolvers import reverse

# Create your models here.
class TimeTrackable(models.Model):
    date_updated = models.DateTimeField('date updated', auto_now=True)
    date_created = models.DateTimeField('date created', auto_now_add=True)

    class Meta:
        abstract = True

class Site(TimeTrackable):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    location = models.CharField(max_length=200)

    def __str__(self):
        return "{}-{}".format(self.description, self.location)

    def get_reports(self):
        return Report.objects.filter(site_id=self.id)

    def get_location_font_color(self):
        if "hot" in self.location.lower():
            return 'red'
        elif "chilled" in self.location.lower():
            return 'blue'
        else:
            return 'black'

    def get_location_icon(self):
        if "hot" in self.location.lower():
            return 'fa fa-fire fa-lg'
        elif "chilled" in self.location.lower():
            return 'fa fa-snowflake-o fa-lg'
        else:
            return 'fa fa-tint fa-lg'

class ReportManager(models.Manager):
    def recent_reports(self, number):
        return self.order_by('-date_created')[:number]


class Report(TimeTrackable):
    site = models.ForeignKey(Site, related_name="%(app_label)s_%(class)s_site")
    remarks = models.TextField()
    report_date = models.DateTimeField()

    objects = ReportManager()

    def __str__(self):
        """ returns string of Report """
        return "{} {}".format(self.report_date, self.site)

    def get_tests(self):
        """ gets all tests belonging to this report """
        return Test.objects.filter(report_id=self.id)

    def get_site(self):
        """ returns site of report """
        return Site.objects.get(id=self.site.id)

    def wipe_tests(self):
        """ deletes all associated tests """
        Test.objects.filter(report_id=self.id).delete()


class Test(TimeTrackable):
    
    DESCRIPTION_CHOICES = (
    ('PAK', 'P Alkalinity'),
    ('MAK', 'M Alkalinity'),
    ('CHL', 'Chlorides'),
    ('HRD', 'Hardness'),
    ('NIT', 'Nitrite'),
    ('PHO', 'Phosphate'),
    ('SUL', 'Sulfite'),
    ('PH', 'Ph'),
    ('SUS', 'Suspended Solids'),
    )

    report = models.ForeignKey(Report, related_name="%(app_label)s_%(class)s_report")
    description = models.CharField(max_length=3, choices=DESCRIPTION_CHOICES)
    measurement = models.CharField(max_length=200)
    residuals = models.CharField(max_length=200)
    parameters = models.CharField(max_length=200, blank=True)

    def __str__(self):
        return "{} {}".format(self.report, self.description)

