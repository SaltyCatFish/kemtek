from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse

from unittest.mock import patch


# Create your tests here.
from ..models import Site, Report, Test
from .utils.factories import SiteFactory

class ReportTestCase(TestCase):
    def setUp(self):
        test_user1 = User.objects.create_user(username='testuser1', password='testuser1')
        test_user1.save()
        client = Client()

    def test_msg_if_no_report_list(self):
        self.client.login(username='testuser1', password='testuser1')
        site = SiteFactory()
        pk = site.pk
        response = self.client.get(reverse('lab_reports:site-detail', args=[pk,]), follow=True)
        #print(response.content)
        self.assertContains(response, 'There are no reports for this site', status_code=200, html=True)

    def test_404_if_no_report(self):
        self.client.login(username='testuser1', password='testuser1')
        site = SiteFactory()
        pk = site.pk
        response = self.client.get(reverse('lab_reports:report-detail', args=[pk,]), follow=True)
        self.assertEqual(response.status_code, 404)

    @patch('lab_reports.models.Site', autospec=True)
    def test_to_string(self, MockSite):
        site = MockSite()
        site._state = MockSite()
        site.db = MockSite()
        site.__str__.return_value = 'site'
        
        report = Report(
            site = site,
            report_date = '2001-01-01'
            )

        self.assertEqual(report.__str__(), '2001-01-01 site')

    @patch('lab_reports.models.Test.objects.filter')
    @patch('lab_reports.models.Site', autospec=True)
    def tests_get_tests_returns_all_tests(self, MockSite, filter):
        site = MockSite()
        site._state = MockSite()
        site.db = MockSite()
        filter.return_value = [1,2,3]
        report = Report(
            site = site,
            report_date = '2001-01-01'
            )
        self.assertEqual(report.get_tests(), [1,2,3])

    @patch('lab_reports.models.Site.objects.get')
    @patch('lab_reports.models.Site', autospec=True)
    def tests_get_site_returns_site(self, MockSite, get):
        site = MockSite()
        site._state = MockSite()
        site.db = MockSite()
        get.return_value = site
        report = Report(
            site = site,
            report_date = '2001-01-01'
            )
        self.assertEqual(report.get_site(), site)

    @patch('lab_reports.models.Test.objects')
    @patch('lab_reports.models.Site', autospec=True)
    def tests_wipe_tests_calls_delete(self, MockSite, objects):
        site = MockSite()
        site._state = MockSite()
        site.db = MockSite()
        report = Report(
            site = site,
            report_date = '2001-01-01'
            )
        objects.filter.return_value = objects
        report.wipe_tests()
        objects.delete.assert_called_once_with()






    








