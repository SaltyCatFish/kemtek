from django.test import TestCase, Client
from django.urls import reverse

# Create your tests here.
from ..models import Site, Report, Test

class SiteTestCase(TestCase):

    def test_to_string(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='location',
            )
        self.assertEqual(site.__str__(), 'description-location')

    def test_location_font_color_when_hot(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Hot Water Boiler',
            )
        self.assertEqual(site.get_location_font_color(), 'red')

    def test_location_font_color_when_cold(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Chilled Water Boiler',
            )
        self.assertEqual(site.get_location_font_color(), 'blue')

    def test_location_icon_when_hot(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Hot Water Boiler',
            )
        self.assertEqual(site.get_location_icon(), 'fa fa-fire fa-lg')

    def test_location_icon_when_cold(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Chilled Water Boiler',
            )
        self.assertEqual(site.get_location_icon(), 'fa fa-snowflake-o fa-lg')







