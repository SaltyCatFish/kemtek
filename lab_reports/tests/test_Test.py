from django.test import TestCase, Client
from django.urls import reverse
from unittest.mock import patch


# Create your tests here.
from ..models import Site, Report, Test

class TestTestCase(TestCase):
    def setup(self):
        client = Client()

    @patch('lab_reports.models.Report', autospec=True)
    def test_to_string(self, MockReport):
        report = MockReport()
        report._state = MockReport()
        report.db = MockReport()
        report.__str__.return_value = 'report'
        
        test = Test(
            report = report,
            description = 'description',
            measurement = 'measurement',
            residuals = 'residuals',
            parameters = 'parameters',
            )

        self.assertEqual(test.__str__(), 'report description')


    