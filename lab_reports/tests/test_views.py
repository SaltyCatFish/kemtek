from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.models import User

from unittest.mock import patch


# Create your tests here
from ..models import Site, Report, Test
from ..views import IndexView, ReportView, ReportDelete
from .utils.factories import SiteFactory, ReportFactory

class SiteViewTestCase(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/site/1', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/site/1', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:site-detail', args=[1,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:site-detail', args=[1,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/site/site_detail.html')

    def test_404_if_no_site(self):
        response = self.client.get(reverse('lab_reports:site-detail', args=[1000,]), follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.get(reverse('lab_reports:site-detail', args=[1000,]), follow=True)
        self.assertEqual(response.status_code, 404)

    def test_contains_site_details(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Hot Water Boiler',
            )
        response = self.client.get(reverse('lab_reports:site-detail', args=[site.id,]), follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.get(reverse('lab_reports:site-detail', args=[site.id,]), follow=True)
        self.assertContains(response, 'name')
        self.assertContains(response, 'description')
        self.assertContains(response, 'Hot Water Boiler')

    def test_detail_report_appears_in_list(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Hot Water Boiler',
            )
        report = ReportFactory(site=site)
        response = self.client.get(reverse('lab_reports:site-detail', args=[site.id]), follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.get(reverse('lab_reports:site-detail', args=[site.id]), follow=True)
        self.assertContains(response, 'Jan 01, 2001')

class SiteListViewTestCase(TestCase):        
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/sites', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/sites', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:site-list'))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:site-list'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/site/site_list.html')

    def test_msg_if_no_site_list(self):
        response = self.client.get(reverse('lab_reports:site-list'), follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.get(reverse('lab_reports:site-list'), follow=True)
        self.assertContains(response, 'No sites available', status_code=200, html=True)

    def test_site_appears_in_list(self):
        site = Site.objects.create(
            name='name',
            description='description',
            location='Hot Water Boiler',
            )
        response = self.client.get(reverse('lab_reports:site-list'), follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.get(reverse('lab_reports:site-list'), follow=True)
        self.assertContains(response, 'name')
        self.assertContains(response, 'Hot Water Boiler')

class LoginViewTestCase(TestCase):

    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/login')
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:login'))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:login'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/login.html')

    def test_for_fields_and_login_button_are_present(self):
        response = self.client.get(reverse('lab_reports:login'))
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Password')
        self.assertContains(response, 'Login')

    def test_succesful_login_redirect(self):
        self.client.login(username='testuser1', password='testuser1')
        response = self.client.post(reverse('lab_reports:login'), {
            'username': 'testuser1',
            'password': 'testuser1',
            }, follow=True)
        self.assertRedirects(response, '/lab_reports/', 302, 200)

    def test_failed_login_displays_error_message(self):
        response = self.client.post(reverse('lab_reports:login'), {
            'username': 'testuser1', 
            'password': 'testuser',
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Please enter a correct username and password')


class IndexViewTestCase(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:index'))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/dashboard.html')


class ReportDetailViewTestCase(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/2', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/report/2', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-detail', args=[2,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-detail', args=[2,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/report/report_detail.html')

class ReportAddViewTestCase(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/2', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/report/2', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-detail', args=[2,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-detail', args=[2,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/report/report_detail.html')

class ReportEditRemarksViewTestCase(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/edit/14/remarks/', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/report/edit/14/remarks/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-edit-remarks', args=[14,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-edit-remarks', args=[14,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/report/report_remarks_form.html')

class ReportDeleteView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/delete/14/', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/report/delete/14/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-delete', args=[14,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-delete', args=[14,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/report/report_confirm_delete.html')

class ReportPdfView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/pdf/5/download', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        """ TODO: Implement this correctly """
        pass
        # self.client.force_login(self.test_user)
        # response = self.client.get('/lab_reports/report/pdf/5/download', follow=True)
        # self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-pdf', args=[5,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:report-pdf', args=[5,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/report/report_print.html')

class ReportDownloadView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/report/pdf/5/download', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 302, 200)

    def test_site_view_url_exists(self):
        """ TODO: Implement this correctly """
        pass
        # self.client.force_login(self.test_user)
        # response = self.client.get('/lab_reports/report/pdf/5/download', follow=True)
        # self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        """ TODO: Implement this correctly """
        pass
        # self.client.force_login(self.test_user)
        # response = self.client.get(reverse('lab_reports:report-download', args=[5,]))
        # self.assertEqual(response.status_code, 200)


class TestAddView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/test/add/5', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/test/add/5', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-add', args=[5,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-add', args=[5,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/test/test_form.html')

class TestEditView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/test/edit/5', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)

    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/test/edit/5', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-edit', args=[7,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-edit', args=[7,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/test/test_form.html')

class TestDeleteView(TestCase):
    
    fixtures = ['default']
     
    def setUp(self):
        self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
        self.test_user.save()

    def test_site_redirects_to_login_if_not_logged_in(self):
        response = self.client.get('/lab_reports/test/delete/8', follow=True)
        self.assertRedirects(response, '/lab_reports/login', 301, 200)
        
    def test_site_view_url_exists(self):
        self.client.force_login(self.test_user)
        response = self.client.get('/lab_reports/test/delete/8', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_url_accessible_by_name(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-delete', args=[8,]))
        self.assertEqual(response.status_code, 200)

    def test_correct_template_used(self):
        self.client.force_login(self.test_user)
        response = self.client.get(reverse('lab_reports:test-delete', args=[8,]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'lab_reports/test/test_confirm_delete.html')



#Template
# class SiteViewTestCase(TestCase):
    
#     fixtures = ['default']
     
#     def setUp(self):
#         self.test_user = User.objects.create_user(username='testuser1', password='testuser1')
#         self.test_user.save()

#     def test_site_redirects_to_login_if_not_logged_in(self):
#         response = self.client.get('/lab_reports/site/1', follow=True)
#         self.assertRedirects(response, '/lab_reports/login', 301, 200)

#     def test_site_view_url_exists(self):
#         self.client.force_login(self.test_user)
#         response = self.client.get('/lab_reports/site/1', follow=True)
#         self.assertEqual(response.status_code, 200)

#     def test_url_accessible_by_name(self):
#         self.client.force_login(self.test_user)
#         response = self.client.get(reverse('lab_reports:site-detail', args=[1,]))
#         self.assertEqual(response.status_code, 200)

#     def test_correct_template_used(self):
#         self.client.force_login(self.test_user)
#         response = self.client.get(reverse('lab_reports:site-detail', args=[1,]))
#         self.assertEqual(response.status_code, 200)
#         self.assertTemplateUsed(response, 'lab_reports/site/site_detail.html')



    
