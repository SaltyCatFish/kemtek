import factory
import datetime
from django.utils import timezone
from factory.django import DjangoModelFactory
from lab_reports.models import Site, Report, Test

class SiteFactory(DjangoModelFactory):
    class Meta:
        model = Site

    name = 'Ficticious School District'
    description = 'MadeUp Academy'
    location = 'Hot Water Boiler'

class ReportFactory(DjangoModelFactory):
    class Meta:
        model = Report

    site = factory.SubFactory(SiteFactory)
    remarks = factory.Faker('sentence')
    report_date = timezone.make_aware(datetime.datetime(2001, 1, 1, 1), timezone.get_default_timezone())

class TestFactory(DjangoModelFactory):
    class meta:
        model = Test

    report = factory.SubFactory(ReportFactory)
    description = 'PAK'
    measurement = 'measurement'
    residual = 'residuals'
    paramteres = 'parameters'

