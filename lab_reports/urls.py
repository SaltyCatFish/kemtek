"""kemtek URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin

from django.contrib.auth import views as auth_views

from wkhtmltopdf.views import PDFTemplateView

from . import views

app_name = "lab_reports"
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name="index"),

    url(r'^login$', views.LoginView.as_view(template_name='registration/login.html'), name='login'),
    
    url(r'^site/(?P<pk>[0-9]+)/$', views.SiteView.as_view(), name='site-detail'),
    url(r'^sites$', views.SiteListView.as_view(), name='site-list'),
    
    url(r'^report/(?P<pk>[0-9]+)/$', views.ReportView.as_view(), name='report-detail'),
    url(r'^report/add/(?P<site_id>[0-9]+)/$', views.ReportCreate.as_view(), name='report-add'),
    url(r'^report/edit/(?P<report_id>[0-9]+)/remarks/$', views.ReportEditRemarksView.as_view(), name='report-edit-remarks'),
    url(r'^report/delete/(?P<pk>[0-9]+)/$', views.ReportDelete.as_view(), name='report-delete'),
    url(r'^report/pdf/(?P<report_id>[0-9]+)/$', views.ReportPrintView.as_view(), name='report-pdf'),
    url(r'^report/pdf/(?P<report_id>[0-9]+)/download$', views.ReportDownloadView.as_view(), name='report-download'),
    
    url(r'^test/add/(?P<report_id>[0-9]+)/$', views.TestAddView.as_view(), name='test-add'),
    url(r'^test/edit/(?P<test_id>[0-9]+)/$', views.TestEdit.as_view(), name='test-edit'),
    url(r'^test/delete/(?P<pk>[0-9]+)/$', views.TestDelete.as_view(), name='test-delete'),
]

