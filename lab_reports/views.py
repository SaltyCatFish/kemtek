import time


from django.shortcuts import get_list_or_404, get_object_or_404, render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import views as auth_views

from django.urls import reverse, reverse_lazy

from django.views import generic, View
from django.views.generic.detail import DetailView
from django.views.generic.edit import DeleteView, CreateView
from django.utils import timezone
from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.shortcuts import render

from django.forms import Textarea

from wkhtmltopdf.views import PDFTemplateView
import pdfkit

from .forms import TestForm, ReportDateForm, ReportRemarksForm, AuthenticationForm
from .models import Site, Report, Test

class LoginRequiredMixin(LoginRequiredMixin):
    login_url = '/login'


class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'lab_reports/dashboard.html'
    context_object_name = 'data'

    def get_queryset(self):
        return {
            "reports": Report.objects.recent_reports(6),
        }

class SiteListView(LoginRequiredMixin, generic.ListView):
    template_name = 'lab_reports/site/site_list.html'
    context_object_name = 'sites'

    def get_queryset(self):
        return Site.objects.all().order_by('description', 'location')

class SiteView(LoginRequiredMixin, DetailView):
    model = Site
    context_object_name = 'site'
    template_name = 'lab_reports/site/site_detail.html'

    def get_context_data(self, **kwargs):
        context = super(SiteView, self).get_context_data(**kwargs)
        context['reports'] = self.get_object().get_reports()
        return context

class ReportView(LoginRequiredMixin, DetailView):
    model = Report
    context_object_name = 'report'
    template_name = 'lab_reports/report/report_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ReportView, self).get_context_data(**kwargs)
        context['tests'] = self.get_object().get_tests()
        return context


class ReportDelete(LoginRequiredMixin, DeleteView):
    model = Report
    template_name = 'lab_reports/report/report_confirm_delete.html'
    def get_success_url(self):
        return reverse_lazy('lab_reports:site-detail', args = [self.get_object().site.id,], current_app='lab_reports')

class ReportCreate(View):
    template_name_field = 'lab_reports/report/report_form.html'
    def post(self, request, *args, **kwargs):
        site = Site.objects.get(pk=kwargs['site_id'])
        form = ReportDateForm(request.POST)
        if form.is_valid():
            report = Report.objects.create(site=site, report_date=form.cleaned_data['report_date'])
            report.save()
            return HttpResponseRedirect('/lab_reports/report/{}'.format(report.id))

    def get(self, request, *args, **kwargs):
        form = ReportDateForm()
        return render(request, 'lab_reports/report/report_form.html', {
        'form': form
        })

class ReportEditRemarksView(LoginRequiredMixin, View):
    template_name_field = 'report_remarks_form.html'

    def post(self, request, *args, **kwargs):
        form = ReportRemarksForm(request.POST)
        if form.is_valid():
            report = get_object_or_404(Report, pk=kwargs['report_id'])
            report.remarks = form.cleaned_data['remarks']
            report.save()
            return HttpResponseRedirect('/lab_reports/report/{}'.format(kwargs['report_id']))

    def get(self, request, *args, **kwargs):
        report = get_object_or_404(Report, pk=kwargs['report_id'])
        form = ReportRemarksForm(initial={'remarks': report.remarks })
        return render(request, 'lab_reports/report/report_remarks_form.html', {
            'form': form
            })


class TestAddView(LoginRequiredMixin, View):

    template_name_field = 'report_remarks_form.html'

    def post(self, request, *args, **kwargs):
        report_id = kwargs['report_id']

        report = Report.objects.get(pk=kwargs['report_id'])
        tests = Test.objects.filter(report__pk=kwargs['report_id']).order_by('description')
        form = TestForm(request.POST)
        report = get_object_or_404(Report, pk=report_id)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.report = report
            obj.save()
            return HttpResponseRedirect('/lab_reports/report/{}'.format(report_id))
        
    def get(self, request, *args, **kwargs):
        form = TestForm(initial={'measurement': 'ppm'})
        tests = Test.objects.filter(report__pk=kwargs['report_id']).order_by('description')
        return render(request, 'lab_reports/test/test_form.html', {
            'form': form,
            'tests': tests
            })

class TestDelete(LoginRequiredMixin, DeleteView):
    template_name='lab_reports/test/test_confirm_delete.html'
    model = Test
    def get_success_url(self):
        return reverse_lazy('lab_reports:report-detail', args = [self.get_object().report.id,], current_app='lab_reports')

class TestEdit(LoginRequiredMixin, View):

    def post(self, request, *args, **kwargs):
        instance = get_object_or_404(Test, pk=kwargs['test_id'])
        report = Report.objects.get(pk=instance.report.id)
        tests = Test.objects.filter(report__pk=report.id).order_by('description')
        if request.method == "POST":
            form = TestForm(request.POST, instance=instance)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect('/lab_reports/report/{}'.format(report.id))

    def get(self, request, *args, **kwargs):
        instance = get_object_or_404(Test, pk=kwargs['test_id'])
        report = Report.objects.get(pk=instance.report.id)
        tests = Test.objects.filter(report__pk=report.id).order_by('description')
        form = TestForm(instance=instance)
        return render(request, 'lab_reports/test/test_form.html', {
            'form': form,
            'tests': tests
            })

class ReportPrintView(View):

    def get(self, request, *args, **kwargs):
        report = get_object_or_404(Report, pk=kwargs['report_id'])
        tests = report.get_tests()
        return render(request, 'lab_reports/report/report_print.html', {
            'report': report,
            'tests': tests
            })

class ReportDownloadView(LoginRequiredMixin, View):
    
    def get(self, request, *args, **kwargs):
        report = get_object_or_404(Report, pk=kwargs['report_id'])
        tests = report.get_tests()

        hostname = request.get_host()
        url = reverse('lab_reports:report-pdf', args = [kwargs['report_id'],], current_app='lab_reports')
        print(url)
        pdf_url = request.build_absolute_uri(url)
        print(pdf_url)
        pdf = pdfkit.from_url(pdf_url, False)
        response = HttpResponse(pdf,content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="{}_{}.pdf"'.format(report.report_date.strftime("%Y-%d-%m"), report.site.description)
        return response

class LoginView(auth_views.LoginView):
    template_name = '/registration/login.html'
    





            


        







    


    








